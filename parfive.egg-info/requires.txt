tqdm
aiohttp

[docs]
sphinx
sphinx-automodapi
sunpy-sphinx-theme

[ftp]
aioftp>=0.17.1

[tests]
pytest
pytest-localserver
pytest-asyncio
pytest-socket
pytest-cov
aiofiles
